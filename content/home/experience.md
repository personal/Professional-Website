+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Data Analyst"
  company = "Michigan State University"
  company_url = "https://nssc.msu.edu/"
  location = "East Lansing, MI"
  date_start = "2019-09-04"
  date_end = ""
  description = """
  Providing data analysis, reporting, and statistical support for programs and intiatives under the Neighborhood Student Success Collaborative (NSSC).
  
  Responsibilities include:
  
  * Obtaining and analyzing student data
  * Creating and deploying assessment plans
  * Building statistical models of student success outcomes
  * Creating customized reports and data visualizations for presentations and reports to internal and external partners
  * Dashboard development for NSSC partners
  * Data management
  """

[[experience]]
  title = "Assistant Professor (Fixed-Term)"
  company = "Michigan State University"
  company_url = "http://linglang.msu.edu/degree-programs/linguistics/"
  location = "East Lansing, MI"
  date_start = "2018-08-15"
  date_end = "2019-05-15"
  description = """
  Taught Linguistics and related courses, served on graduate student committees, and supervised research intiatives utilizing self-paced reading, eye-tracking, and EEG.
  
  Research Interests:
  
  * Complement Coercion
  * Reflexive Processing
  * Minimalist Syntactic Theory
  
  
  Classes Taught:
  
  * Introduction to Linguistics
  * Introduction to Sociolinguistics
  * Introduction to Cognitive Science
  """
[[experience]]
  title = "Post-Doctoral Researcher"
  company = "University of Iceland"
  company_url = "https://english.hi.is/"
  location = "Reykjavik, Iceland"
  date_start = "2016-09-04"
  date_end = "2018-02-28"
  description = """
  
  """
+++
