---
authors:
- admin
bio: I analyze data surrounding student success in the Neighborhood Student Success Collaborative at MSU.
education:
  courses:
  - course: PhD in Linguistics
    institution: Michigan State University
    year: 2016
  - course: BA in Spanish
    institution: Michigan State University
    year: 2007
  - course: BA in Linguistics
    institution: Michigan State University
    year: 2007
email: ""
interests:
- Student Success
- Statistical Analysis
- Sytactic Structure in Language
- Data Visualization 
name: Joe Jalbert, Ph.D.
organizations:
- name: Michigan State University
  url: "https://msu.edu/"
- name: Neighborhood Student Success Collaborative
  url: "https://nssc.msu.edu/index.html"
role: Data Analyst
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/sirchatters
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/josephjalbert/
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.msu.edu/jalbertj
superuser: true
user_groups:
- Researchers
- Visitors
---

I am the data analyst for the Neighborhood Student Success Collaborative (NSSC) at Michigan State University. I focus on supporting programs in their internal assessment, including tracking student success outcomes, identifying gaps in outcomes for underserved and minoritized populations, and creating reports.

I employ statistical analyses including propensity score matching, regression, and mixed effects modeling in R. I am developing my skills in creating R packages, as well as in dashboard creation using Shiny.  I am also developing machine learning models to identify predictors of student success that typical admissions criterion ignore.
