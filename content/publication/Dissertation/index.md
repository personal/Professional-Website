---
abstract: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere tellus
  ac convallis placerat. Proin tincidunt magna sed ex sollicitudin condimentum. Sed
  ac faucibus dolor, scelerisque sollicitudin nisi. Cras purus urna, suscipit quis
  sapien eu, pulvinar tempor diam. Quisque risus orci, mollis id ante sit amet, gravida
  egestas nisl. Sed ac tempus magna. Proin in dui enim. Donec condimentum, sem id
  dapibus fringilla, tellus enim condimentum arcu, nec volutpat est felis vel metus.
  Vestibulum sit amet erat at nulla eleifend gravida.
authors:
- Joseph Jalbert
date: "2016-12-16T00:00:00Z"
doi: ""
featured: true
image:
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/s9CC2SKySJM)'
  focal_point: ""
  preview_only: false

projects:
- internal-project
publication: Michigan State University Dissertation
publication_short: "MSUDISS"
publication_types:
- "7"
publishDate: "2016-12-16T00:00:00Z"

summary: My dissertation examining the how the interactions between the contraints on the derivation and minimalist assumptions can lead to important insights to long-standing problems in syntactic theory including relative clauses and parasitic gaps
tags:
- Syntax
- Sideward Movement
- Parasitic Gaps
- Relative Clauses
- Minimalist
- English
title: Restricting sideward movement
subtitle: The course of the derivation and the directionality of movement
url_code: ''
url_dataset: ''
url_pdf: https://pdfs.semanticscholar.org/d06e/874d2c6546901093d45336c0149ee9ce25e1.pdf
url_poster: ''
url_project: ""
url_slides: ""
url_source: ''
url_video: ''
---

