---
abstract: Recent research on noun–noun compounds has suggested that the parser may commit to the first noun (N1) as the head and then have to revise that commitment when the second noun (N2) is encountered. However, it remains unclear under what circumstances head commitment at N1 occurs, and whether it involves both semantic and syntactic revisions at N2. In two event-related potential experiments in German, we explored these questions by manipulating gender matches between a determiner and N1/N2 in compounds. In Experiment 1, a determiner–noun match in gender at N1 compared with a mismatch yielded an effect at N2 for the matching condition (increased negativity at 480–550 ms, strongest in the left hemisphere); there was a similar effect for the gender violation at N2. The observed negativity could have been due to either semantic or syntactic head revision, or both. Experiment 2 increased expectations that an N2 was imminent, which attenuated syntactic, but not semantic, effects at N2. We found N400-like effects, often associated with semantic integration, suggesting that, in Experiment 1, the syntactic effects had masked the semantic costs. Taken together, these results support the idea that both semantic and syntactic head commitment and revision occur during compound processing.
authors:
- Joseph Jalbert
- Tyler Roberts
- Alan Beretta
date: "2016-03-10T00:00:00Z"
doi: "10.1097/WNR.0000000000000520"
featured: true
image:
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/3KGF9R_0oHs)'
  focal_point: ""
  preview_only: false
projects:
- internal-project
publication: In *NeuroReport*
publication_short: In *NR*
publication_types:
- "1"
publishDate: "2016-03-10T00:00:00Z"
summary: Paper examining the neurophysiological correlates of compound processing.
tags:
- Neurolinguistics
- Compound Processing
- German
title: Neurophysiological effects of prediction on head reassignment in German compounds
url_pdf: https://journals.lww.com/neuroreport/Abstract/2016/02010/Neurophysiological_effects_of_prediction_on_head.9.aspx
---


