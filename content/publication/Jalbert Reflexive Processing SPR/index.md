---
abstract: This paper presents a study of the processing differences found between anaphors and logophors. Previous research by Harris et al. (2000) has suggested that SELF anaphors (as described by Reinhart and Reuland (R&R) (1993) exhibit P600 effects when they do not meet the structural requirements to be bound by their antecedent. Reflexives that are not governed by R&R's version of binding (logophors), however, do not exhibit the same P600 effect when illicitly licensed. Harris et al. take this as evidence that R&R's theory of binding is correct, due to the lack of syntactic ERP effects for logophors (which they predict would be licensed by semantic/discourse considerations). I present evidence that this lack of effect is due to a different property that logophors have, which is a sensitivity to intrusive antecedents (Xiang et al. 2009, King et al. 2012), which is not shared by anaphors. In experiment 1, I reproduce a processing effect for anaphors and the lack of processing effect for mismatched logophors in a self-paced reading task. In experiment 2, I show that when intrusive antecedents are removed from those sentences, reading slowdowns are found in for both mismatched anaphors and logophors. While the results in experiment 1 and Harris et al.'s experiment provide compelling processing evidence for the conceptual distinction between anaphors and logophors, experiment 2 shows that concluding that this is due to the lack of syntactic processing is unwarranted.
authors:
- Joseph Jalbert
date: "2018-04-02T00:00:00Z"
doi: ""
featured: false
image:
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/k_T9Zj3SE8k)'
  focal_point: ""
  preview_only: false
projects: []
publication: '*University of Pennsylvania Working Papers in Linguistics'
publication_short: "pwpl"
publication_types:
- "2"
publishDate: "2018-04-02T00:00:00Z"
summary: Self-Paced Reading study examining the interaction between the processing of reflexives and intrusive antecedents.
tags: 
- Self-Paced Reading
- Reflexives
- Intrusive Antecedents
- English
title: Differential Processing of Anaphors and Logophors in Self-Paced Reading
url_code: ""
url_dataset: ""
url_pdf: https://repository.upenn.edu/cgi/viewcontent.cgi?article=2006&context=pwpl
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

